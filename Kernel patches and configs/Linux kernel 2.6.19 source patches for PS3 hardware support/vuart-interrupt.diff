Index: linux/arch/powerpc/platforms/ps3pf/Makefile
===================================================================
--- linux.orig/arch/powerpc/platforms/ps3pf/Makefile	2006-10-02 15:18:23.921826438 +0900
+++ linux/arch/powerpc/platforms/ps3pf/Makefile	2006-10-02 15:18:23.962825886 +0900
@@ -1,4 +1,6 @@
 obj-y	+= pic.o htab.o setup.o hv.o irq.o
 
+obj-$(CONFIG_PS3PF_VUART) += vuart.o
+
 sb-objs = pci.o pci_res.o
 obj-$(CONFIG_PPC_PS3PF_SB)	+= sb.o
Index: linux/arch/powerpc/platforms/ps3pf/vuart.c
===================================================================
--- /dev/null	1970-01-01 00:00:00.000000000 +0000
+++ linux/arch/powerpc/platforms/ps3pf/vuart.c	2006-10-02 15:18:23.963825872 +0900
@@ -0,0 +1,198 @@
+/*
+ * vuart.c
+ *
+ *  Virtual UART interrupt handling
+ *
+ * Copyright (C) 2006 Sony Computer Entertainment Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU General Public License as published
+ * by the Free Software Foundation; version 2 of the License.
+ *
+ * This program is distributed in the hope that it will be useful, but
+ * WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ * General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License along
+ * with this program; if not, write to the Free Software Foundation, Inc.,
+ * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+ */
+
+#include <linux/config.h>
+#include <linux/module.h>
+#include <linux/notifier.h>
+#include <linux/reboot.h>
+#include <asm/io.h>
+#include <asm/lv1call.h>
+#include "vuart.h"
+
+extern unsigned long p_to_lp(long pa);
+
+static u64 vuart_bitmap[4] = {0, 0, 0, 0};
+static unsigned long	vuart_irq_outlet_id;
+static unsigned int	vuart_irq_no;
+static unsigned int	vuart_irq_cpu;
+static vuart_callback_t	vuart_callbacks[3] = { NULL, NULL, NULL };
+
+
+static int ps3pf_connect_vuart_irq(
+	unsigned long	*outlet_id,
+	unsigned int	*irq_no,
+	unsigned int	*irq_cpu,
+
+	/* arguments to request_irq() */
+	irqreturn_t	(*handler)(int, void *, struct pt_regs *),
+	unsigned long	flags,
+	const char	*irq_name,
+	void		*dev_id
+	)
+{
+	long				status;
+
+	status = lv1_configure_virtual_uart_irq(
+		p_to_lp(virt_to_phys(&vuart_bitmap)),
+		outlet_id);
+	if (unlikely(status)) {
+		printk(KERN_ERR "%s(): construct / get spu irq outlet :"
+		       "failed: %ld\n", __FUNCTION__, status);
+		return -EPERM;
+	}
+
+	return __ps3pf_connect_irq(*outlet_id,
+				   irq_no, irq_cpu,
+				   handler, flags, irq_name, dev_id);
+}
+
+static int ps3pf_free_vuart_irq(
+	unsigned long	outlet_id,
+	unsigned int	irq_no,
+	unsigned int	irq_cpu,
+	void		*dev_id
+	)
+{
+	int ret;
+	long status;
+
+	ret = __ps3pf_free_irq(outlet_id, irq_no, irq_cpu, dev_id);
+
+	status = lv1_deconfigure_virtual_uart_irq();
+	if (unlikely(status)) {
+		printk(KERN_ERR "%s(): lv1_deconfigure_virtual_uart_irq() "
+		       "failed: %ld\n", __FUNCTION__, status);
+		ret = -EPERM;
+	}
+	return ret;
+}
+
+int vuart_set_callback(unsigned int port, vuart_callback_t func)
+{
+	if (port >= 3)
+		return -EPERM;
+
+	vuart_callbacks[port] = func;
+	return 0;
+}
+EXPORT_SYMBOL_GPL(vuart_set_callback);
+
+int vuart_set_recv_watermark(unsigned int port, unsigned long size)
+{
+	unsigned long st;
+	unsigned long max;
+
+	st = lv1_get_virtual_uart_param(
+		port,
+		VUART_PARAM_RECV_BUF_SIZE_MAX,
+		&max);
+	if (unlikely(st)) {
+		printk(KERN_ERR "VUART: failed to get recv buf size\n");
+		return -EPERM;
+	}
+
+	st = lv1_set_virtual_uart_param(
+		port,
+		VUART_PARAM_RECV_HIGH_WATERMARK,
+		max - size);
+	if (unlikely(st)) {
+		printk(KERN_ERR "VUART: failed to set recv watermark\n");
+		return -EPERM;
+	}
+	return 0;
+}
+EXPORT_SYMBOL_GPL(vuart_set_recv_watermark);
+
+int vuart_set_irq_mask(unsigned int port, unsigned long mask)
+{
+	unsigned long st;
+
+	st = lv1_set_virtual_uart_param(port, VUART_PARAM_IRQ_MASK, mask);
+	if (unlikely(st)) {
+		printk(KERN_ERR "VUART: failed to set irq mask\n");
+		return -EPERM;
+	}
+	return 0;
+}
+EXPORT_SYMBOL_GPL(vuart_set_irq_mask);
+
+static irqreturn_t vuart_event_handler(int irq, void *data,
+				       struct pt_regs *regs)
+{
+	int q;
+	int port;
+	u64 bits;
+
+	for (q = 0; q < 4; q++) {\
+		bits = vuart_bitmap[q];
+		port = q * 64;
+		while (bits != 0) {
+			if (bits & 0x8000000000000000UL) {
+				if (port < 3  && vuart_callbacks[port]) {
+					vuart_callbacks[port]();
+				} else {
+					printk(KERN_ERR "VUART: interrupted by unknown port(port=%d)\n", port);
+					vuart_set_irq_mask(port, 0);
+				}
+			}
+			bits <<= 1;
+			port++;
+		}
+	}
+
+	return IRQ_HANDLED;
+}
+
+static int vuart_at_exit(struct notifier_block *self, unsigned long state,
+			 void *data)
+{
+	printk(KERN_INFO "VUART: cleanup.\n");
+	ps3pf_free_vuart_irq(vuart_irq_outlet_id,
+			     vuart_irq_no,
+			     vuart_irq_cpu,
+			     NULL);
+	return NOTIFY_OK;
+}
+
+static struct notifier_block vuart_reboot_nb = {
+	.notifier_call = vuart_at_exit
+};
+
+int __init vuart_init(void)
+{
+	int res;
+
+	printk(KERN_INFO "VUART: initialize\n");
+
+	res = ps3pf_connect_vuart_irq(&vuart_irq_outlet_id,
+				      &vuart_irq_no,
+				      &vuart_irq_cpu,
+				      &vuart_event_handler,
+				      0,
+				      "Virtual UART",
+				      NULL);
+
+	if (likely(res == 0)) {
+		register_reboot_notifier(&vuart_reboot_nb);
+	}
+	return res;
+}
+__initcall(vuart_init);
Index: linux/arch/powerpc/platforms/ps3pf/vuart.h
===================================================================
--- /dev/null	1970-01-01 00:00:00.000000000 +0000
+++ linux/arch/powerpc/platforms/ps3pf/vuart.h	2006-10-02 15:18:23.963825872 +0900
@@ -0,0 +1,34 @@
+/*
+ * Copyright (C) 2006 Sony Computer Entertainment Inc.
+ *
+ * This program is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU General Public License as published
+ * by the Free Software Foundation; version 2 of the License.
+ *
+ * This program is distributed in the hope that it will be useful, but
+ * WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+ * General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License along
+ * with this program; if not, write to the Free Software Foundation, Inc.,
+ * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+ */
+
+#define VUART_PARAM_SEND_LOW_WATERMARK	0
+#define VUART_PARAM_RECV_HIGH_WATERMARK	1
+#define VUART_PARAM_IRQ_MASK		2
+#define VUART_PARAM_RECV_BUF_SIZE_MAX	3
+#define VUART_PARAM_RECV_BUF_SIZE	4
+#define VUART_PARAM_SEND_BUF_SIZE_MAX	5
+#define VUART_PARAM_SEND_BUF_SIZE	6
+
+#define VUART_IRQ_MASK_SEND		(1UL<<(63-63))
+#define VUART_IRQ_MASK_RECV		(1UL<<(63-62))
+#define VUART_IRQ_MASK_DISCONNECT	(1UL<<(63-61))
+
+typedef void (*vuart_callback_t)(void);
+
+int vuart_set_callback(unsigned int port, vuart_callback_t func);
+int vuart_set_recv_watermark(unsigned int port, unsigned long size);
+int vuart_set_irq_mask(unsigned int port, unsigned long mask);
Index: linux/arch/powerpc/platforms/ps3pf/Kconfig
===================================================================
--- linux.orig/arch/powerpc/platforms/ps3pf/Kconfig	2006-10-02 15:18:23.920826451 +0900
+++ linux/arch/powerpc/platforms/ps3pf/Kconfig	2006-10-02 15:18:23.963825872 +0900
@@ -1,6 +1,11 @@
 menu "PS3PF platform options"
 	depends on PPC_PS3PF
 
+config PS3PF_VUART
+	depends on PPC_PS3PF
+	bool "Virtual UART interrupt support"
+	default n
+
 config BRINGUP_MSG
 	depends on PPC_PS3PF
 	bool "Verbose messages for bringup activities"
