Subject: powerpc: basic systemsim support

Add some support and doc files for the IBM Full System
Simulator and a configuration option that acts as a control
for the patches based on this one.

No code is changed in this patch, but it is required for
the systemsim network, block, hvc_console and cpu_idle
patches that are based on the infrastructure in here.

From: Eric Van Hensbergen <ericvh@gmail.com>
Signed-off-by: Arnd Bergmann <arndb@de.ibm.com>
---

Index: linus-2.6/include/asm-powerpc/systemsim.h
===================================================================
--- /dev/null
+++ linus-2.6/include/asm-powerpc/systemsim.h
@@ -0,0 +1,132 @@
+/*
+ *
+ * Copyright (C) 2001, 2005 IBM
+ *
+ * Filename	: systemsim.h
+ *
+ * Originator	: Patrick Bohrer and Charles Lefurgy
+ * Modified By	: Eric Van Hensbegren <ericvh@gmail.com>
+ *
+ * Purpose	:
+ *
+ *   This file is compiled with programs that are run under the
+ *   PowerPC Full System simulator.  For example, stand-alone programs
+ *   or operating systems.  The programs call the callthru wrapper
+ *   functions which use an illegal PowerPC instruction to signal the
+ *   simulator to emulate special support.
+ *
+ */
+
+#ifndef _SYSTEMSIM_CONFIG_H_
+#define _SYSTEMSIM_CONFIG_H_
+#ifdef __KERNEL__
+
+/* The functions callthru0 to callthru5 setup up the arguments for the
+ * simulator callthru and then use the callthru instruction.  Note that
+ * 0-5 specify the number of arguments after the command */
+
+/* Note: Arguments are cast as void* to prevent casting by the
+   compiler.  This way, you can pass pointers, integers, etc. in
+   machine register and have the simulator interpret what the
+   register is supposed to be.  To help with typing errors when using
+   callthrus, we provide wrapper functions for each callthru.  The
+   wrappers cast all arguments to void*.  Unfortunately, this results
+   in a lot of compiler warnings that I do not know how to remove.  If
+   you modify this code, be aware that we are trying to pick a type
+   that is the size of the registers (32-bit or 64-bit) and that is
+   why are choosing to cast to a void* (it should be the size of a
+   machine register) */
+
+static inline int callthru0(int command)
+{
+	register int c asm("r3") = command;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c));
+	return (c);
+}
+static inline int callthru1(int command, unsigned long arg1)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1));
+	return (c);
+}
+static inline int callthru2(int command, unsigned long arg1, unsigned long arg2)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	register unsigned long a2 asm("r5") = arg2;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1), "r"(a2));
+	return (c);
+}
+static inline int callthru3(int command, unsigned long arg1, unsigned long arg2,
+			    unsigned long arg3)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	register unsigned long a2 asm("r5") = arg2;
+	register unsigned long a3 asm("r6") = arg3;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1), "r"(a2),
+		      "r"(a3));
+	return (c);
+}
+static inline int callthru4(int command, unsigned long arg1, unsigned long arg2,
+			    unsigned long arg3, unsigned long arg4)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	register unsigned long a2 asm("r5") = arg2;
+	register unsigned long a3 asm("r6") = arg3;
+	register unsigned long a4 asm("r7") = arg4;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1), "r"(a2),
+		      "r"(a3), "r"(a4));
+	return (c);
+}
+static inline int callthru5(int command, unsigned long arg1, unsigned long arg2,
+			    unsigned long arg3, unsigned long arg4,
+			    unsigned long arg5)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	register unsigned long a2 asm("r5") = arg2;
+	register unsigned long a3 asm("r6") = arg3;
+	register unsigned long a4 asm("r7") = arg4;
+	register unsigned long a5 asm("r8") = arg5;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1), "r"(a2),
+		      "r"(a3), "r"(a4), "r"(a5));
+	return (c);
+}
+static inline int callthru6(int command, unsigned long arg1, unsigned long arg2,
+			    unsigned long arg3, unsigned long arg4,
+			    unsigned long arg5, unsigned long arg6)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	register unsigned long a2 asm("r5") = arg2;
+	register unsigned long a3 asm("r6") = arg3;
+	register unsigned long a4 asm("r7") = arg4;
+	register unsigned long a5 asm("r8") = arg5;
+	register unsigned long a6 asm("r9") = arg6;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1), "r"(a2),
+		      "r"(a3), "r"(a4), "r"(a5), "r"(a6));
+	return (c);
+}
+static inline int callthru7(int command, unsigned long arg1, unsigned long arg2,
+			    unsigned long arg3, unsigned long arg4,
+			    unsigned long arg5, unsigned long arg6,
+			    unsigned long arg7)
+{
+	register int c asm("r3") = command;
+	register unsigned long a1 asm("r4") = arg1;
+	register unsigned long a2 asm("r5") = arg2;
+	register unsigned long a3 asm("r6") = arg3;
+	register unsigned long a4 asm("r7") = arg4;
+	register unsigned long a5 asm("r8") = arg5;
+	register unsigned long a6 asm("r9") = arg6;
+	register unsigned long a7 asm("r10") = arg7;
+	asm volatile (".long 0x000eaeb0":"=r" (c):"r"(c), "r"(a1), "r"(a2),
+		      "r"(a3), "r"(a4), "r"(a5), "r"(a6), "r"(a7));
+	return (c);
+}
+
+#endif /* __KERNEL__ */
+#endif/* _SYSTEMSIM_CONFIG_H_ */
Index: linus-2.6/arch/powerpc/Kconfig
===================================================================
--- linus-2.6.orig/arch/powerpc/Kconfig
+++ linus-2.6/arch/powerpc/Kconfig
@@ -407,6 +407,19 @@ config PPC_IBM_CELL_BLADE
 	select MMIO_NVRAM
 	select PPC_UDBG_16550
 
+config PPC_SYSTEMSIM
+	bool "  IBM Full System Simulator (systemsim) support"
+	select PPC_CELL_NATIVE
+	select PPC_RTAS
+	select MMIO_NVRAM
+	help
+	  Support booting resulting image under IBMs Full System Simulator.
+	  If you enable this option, you are able to select device
+	  drivers (e.g. network and disk) for purely simulated devices
+	  that are provided by systemsim.
+	  The Full system simulator is available for download from
+	  http://www.alphaworks.ibm.com/tech/.
+
 config XICS
 	depends on PPC_PSERIES
 	bool
Index: linus-2.6/Documentation/systemsim/maple.tcl
===================================================================
--- /dev/null
+++ linus-2.6/Documentation/systemsim/maple.tcl
@@ -0,0 +1,95 @@
+puts "To debug mambo with gdb: attach [pid]"
+#gets stdin
+#simdebug set UART 1
+
+#  get the general procedures defined for mambo
+source $env(LIB_DIR)/ppc/util.tcl
+
+
+#
+#   First create a simulator called mysim
+#
+define dup gpul myconf
+myconf config cpus 1
+myconf config memory_size 128M
+
+# we prefer a maple right now
+myconf config pic/start 0xf8040000
+myconf config pic/end 0xf807ffff
+myconf config pic/little_endian 0
+
+set isa_base 0xf4000000
+
+myconf config rtc/start [add64 $isa_base 0x900]
+myconf config rtc/end [add64 $isa_base 0x90f]
+myconf config uart0 on
+myconf config uart0/start [add64 $isa_base 0x3f8]
+myconf config uart0/end [add64 $isa_base 0x3ff]
+myconf config uart1/start [add64 $isa_base 0x2f8]
+myconf config uart1/end [add64 $isa_base 0x2ff]
+
+define machine myconf mysim
+
+set root [ mysim of find_device / ]
+
+set ht  [ mysim of addchild $root ht 0 ]
+set range [list 0x0 0x0 ]
+mysim of addprop $ht array "bus-range" range
+set ranges [list 0x81000000 0x00000000 0x00000000 0x00000000	         0xf4000000 0x00000000 0x00400000 0x82000000	         0x00000000 0x80000000 0x00000000 0x80000000	         0x00000000 0x70000000 ]
+mysim of addprop $ht array "ranges" ranges
+set reg [list 0x0 0xf2000000 0x03000000 ]
+mysim of addprop $ht array "reg" reg
+mysim of addprop $ht string "compatible" "u3-ht"
+mysim of addprop $ht int "#size-cells" 2
+mysim of addprop $ht int "#address-cells" 3
+mysim of addprop $ht string "device_type" "ht"
+mysim of addprop $ht string "name" "ht"
+
+set isa  [ mysim of addchild $ht isa 4 ]
+mysim of addprop $isa string "name" "isa"
+mysim of addprop $isa string "device_type" "isa"
+set reg [list 0x2000 0x0 0x0 0x0 0x0]
+mysim of addprop $isa array "reg" reg
+set ranges [list 0x1 $isa_base 0x10000 ]
+mysim of addprop $isa array "ranges" ranges
+
+set rtc [ mysim of addchild $isa rtc 0x900 ]
+mysim of addprop $rtc string "compatible" "pnpPNP,B00"
+mysim of addprop $rtc string "name" "rtc"
+mysim of addprop $rtc string "device_type" "rtc"
+set reg  [list 0x1 0x900 0x1 0x1 0x901 0x1]
+mysim of addprop $rtc array "reg" reg
+
+set uart1 [ mysim of addchild $isa serial 0x3f8 ]
+set reg [ list 0x1 0x3f8 0x8 ]
+mysim of addprop $uart1 array "reg" reg
+mysim of addprop $uart1 string "device_type" "serial"
+mysim of package_to_path $uart1
+
+if { [ info exists GUI_HOOK ] } then { eval $GUI_HOOK }
+
+#
+#   Now load the boot image
+#
+
+mysim bogus disk init 0 rootdisk.img newcow simple.cow 1024
+mysim bogus net init 0 de:ad:ca:fe:ba:be /tmp/serversocketh0 0 0
+mysim load vmlinux vmlinux 0
+
+mysim modify loose on
+
+set usertcl /dev/null
+if { [ file exists ./user.tcl ] } {
+   set usertcl ./user.tcl
+}
+source $usertcl
+
+set of_root [mysim of find_device /]
+mysim of setprop $of_root compatible "Momentum,Maple"
+set of_chosen [mysim of find_device /chosen]
+mysim of setprop $of_chosen bootargs "rw root=/dev/mambobd0 console=bogus0"
+simdebug set PCI 1
+set dart [ mysim of addchild $root dart 0 ]
+mysim of addprop $dart string "device_type" "dart"
+mysim of addprop $dart string "compatible" "u3-dart"
+mysim go
Index: linus-2.6/Documentation/systemsim/systemsim.txt
===================================================================
--- /dev/null
+++ linus-2.6/Documentation/systemsim/systemsim.txt
@@ -0,0 +1,92 @@
+                SystemSim: PowerPC Full-System Simulator
+
+Introduction
+============
+
+What is the IBM Full-System Simulator for IBM PowerPC 970?
+----------------------------------------------------------
+
+The IBM Full-System Simulator has been developed and refined in conjunction
+with several large-system design projects built upon the IBM Power Architecture.
+As an execution-driven, full-system simulator, the IBM Full-System Simulator has
+facilitated the experimentation and evaluation of a wide variety of system
+components for core IBM initiatives, such as the STI Cell and the IBM PERCS
+projects. The IBM Full-System Simulator for PowerPC 970 enables development
+teams both within and outside IBM to simulate a PowerPC 970 system in order to
+develop and enhance application support for this platform.
+
+This technology runs on AIX®, Linux®, and Mac OS X (see the requirements
+section on the website for details).
+
+How does it work?
+-----------------
+
+Written in C, a significant part of the Full-System Simulator's simulation
+capability is directly attributed to its TSIM component. Developed as a
+robust, high-performance alternative to conventional process and thread
+programming, TSIM is a light-weight, multi-tasking scheduling framework that
+provides a complete set of facilities for creating and scheduling threads,
+manipulating time delays, and applying a variety of inter-thread communication
+policies and mechanisms to simulation events.
+
+In TSIM's multi-threaded approach, individually-schedulable threads are created
+to simulate a component or collection of components. If a thread must defer
+processing, it requests to be blocked and continues execution when awakened. Any
+local (stack) state is preserved in order to mitigate the necessity for
+allocating areas for saving and restoring this state. TSIM threads are
+non-preemptive; they block only on well-defined events. TSIM simplifies access
+to shared data that must otherwise be serialized in order to guarantee correct
+behavior. Each thread has an associated priority. Scheduling and resource
+allocations are all "first-come, first-served" (FCFS) within priority. TSIM also
+includes software components useful for modeling common hardware components
+such as gates, latches, and ports.
+
+This low-level simulation infrastructure is complemented with a host of
+additional features, such as integrated development and debugging tools,
+support for stand-alone and operating system boot, data collection and
+analysis frameworks, performance visualization, and tracing and logging
+capabilities that enable developers to realistically represent an entire
+system of equipment, devices, or subsystems and simulate not only the
+instructions executed by the processor core, but also its interactions with
+its surrounding system components. Additionally, the Full-System Simulator is
+capable of booting K42, IBM's research operating system, and rHype, the
+research hypervisor.
+
+About the technology author(s):
+-------------------------------
+
+The IBM Full-System Simulator has been developed by the Future Systems group at
+the IBM Austin Research Lab (ARL). Consisting of the Performance and Tools team,
+Novel Systems Architecture team, and Power-Aware Systems team, Future Systems
+focuses on software system issues, including system-level power analysis, system
+performance, and low-level system software issues. The researchers bring a wide
+variety of expertise and extensive experience in numerous software and hardware
+technologies to the team, including computer architecture, performance analysis,
+operating systems, power management, and algorithms.
+
+Where can I get it?
+-------------------
+
+The IBM PowerPC Full-System Simulator is available from the IBM Alphaworks
+website: http://www.alphaworks.ibm.com/tech/systemsim970
+
+Compiling & Booting Linux for SystemSim
+=======================================
+
+When compiling Linux to run on SystemSim you should use the configuration
+file in arch/ppc64/configs/systemsim_defconfig as a base.
+
+When booting the resulting image in SystemSim use the maple.tcl file in
+this directory (Documentation/systemsim/maple.tcl) as a base.  You will
+likely need to change the name of the root disk image and perhaps the path
+to the vmlinux file.  This standard profile will be included with the
+SystemSim tool in the near future (at which point we will hopefully update
+this document.
+
+For further information on using Linux with SystemSim, please reference the
+SystemSim documentation, and the FAQ and/or Forums on the Alphaworks site.
+
+
+
+
+
